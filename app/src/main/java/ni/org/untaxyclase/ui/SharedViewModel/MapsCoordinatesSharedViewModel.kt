package ni.org.untaxyclase.ui.SharedViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MapsCoordinatesSharedViewModel: ViewModel() {
    val data = MutableLiveData<String>()
}