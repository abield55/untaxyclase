package ni.org.untaxyclase.ui.maps

import android.Manifest
import android.content.ContentProviderClient
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.pusher.client.channel.Channel

import ni.org.untaxyclase.R
import ni.org.untaxyclase.ui.SharedViewModel.MapsCoordinatesSharedViewModel
import org.json.JSONObject

class MapFragment : Fragment(), OnMapReadyCallback {
    private var mMap: GoogleMap? = null
    private val ACCESS_FINE_LOCATION_REQUEST_CODE: Int = 5000
    private lateinit var markerOptions: MarkerOptions
    private var marker: Marker? = null
    private lateinit var cameraPosition: CameraPosition
    var defaultLongitude = -122.088426
    var defaultLatitude = 37.3888064
    private var mLocationPermisionGrated: Boolean = false
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var lastKnowLocation: Location

    private val KEY_CAMERA_POSITION = "camera_position"
    private val KEY_LOCATION = "location"

    private lateinit var mapsCoordinatesSharedViewModel: MapsCoordinatesSharedViewModel
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getLocationPermission()
        markerOptions = MarkerOptions()

        if (savedInstanceState != null) {
            lastKnowLocation = savedInstanceState.getParcelable<Location>(KEY_LOCATION)!!
            cameraPosition = savedInstanceState.getParcelable<CameraPosition>(KEY_CAMERA_POSITION)!!
        } else {
            val latLng = LatLng(defaultLatitude, defaultLongitude)
            markerOptions.position(latLng)
            cameraPosition = CameraPosition.Builder()
                .target(latLng)
                .zoom(17f)
                .build()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mLocationPermisionGrated = false
        when (requestCode) {
            ACCESS_FINE_LOCATION_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    mLocationPermisionGrated = true
                    startServiceLocation()
                }
            }
        }
        return
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_map, container, false)

        val supportMapFragment: SupportMapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        supportMapFragment.getMapAsync(this)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mLocationPermisionGrated) {
            activity?.let {
                mapsCoordinatesSharedViewModel =
                    ViewModelProviders.of(it).get(MapsCoordinatesSharedViewModel::class.java)
            }
            mapsCoordinatesSharedViewModel.data.observe(this, Observer {
                it?.let {
                    Log.d("En Fragment", it)
                    val jsonObject = JSONObject(it)
                    if (jsonObject.getString("type").equals("coordinates")) {
                        val coordinates = JSONObject(jsonObject.getString("data"))
                        val latitude = coordinates.getDouble("latitude")
                        val longitude = coordinates.getDouble("longitude")
                        updateMarkerLocation(LatLng(latitude, longitude))
                    }
                }
            })
        }
    }

    private fun updateMarkerLocation(latLng: LatLng) {
        markerOptions.position(latLng)

        cameraPosition = CameraPosition.Builder()
            .target(latLng)
            .zoom(17f)
            .build()

        moveCamera()
        updateMarkerPosition(latLng)
    }

    private fun updateMarkerPosition(latLng: LatLng){
        marker?.position = latLng
    }

    private fun moveCamera() {
        mMap?.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    override fun onMapReady(map: GoogleMap?) {
        mMap = map

        marker = mMap?.addMarker(markerOptions)
        marker?.isDraggable = true
        mMap?.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

        updateLocationUI()
        getDeviceLocation()
    }

    private fun updateLocationUI() {
        if (mMap == null)
            return
        try {
            if (mLocationPermisionGrated) {
                mMap?.isMyLocationEnabled = true
                mMap?.uiSettings?.isMyLocationButtonEnabled = true
            } else {
                mMap?.isMyLocationEnabled = false
                mMap?.uiSettings?.isMyLocationButtonEnabled = false
                getLocationPermission()
            }
        } catch (e : SecurityException) {
            e.printStackTrace()
        }
    }

    private fun getDeviceLocation() {
        try {
            if (mLocationPermisionGrated) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(requireActivity()){
                    if (it.isSuccessful) {
                        it.result?.let {
                            lastKnowLocation = it
                            mMap?.moveCamera(CameraUpdateFactory.newLatLng(
                                LatLng(lastKnowLocation.latitude, lastKnowLocation.longitude)
                            ))
                            updateMarkerLocation(LatLng(lastKnowLocation.latitude, lastKnowLocation.longitude))
                        }
                    } else {
                        //current location is null
                        mMap?.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                        mMap?.uiSettings?.isMyLocationButtonEnabled = false
                        updateMarkerLocation(LatLng(defaultLatitude, defaultLongitude))
                    }
                }
            }
        } catch (exception: SecurityException) {
            exception.printStackTrace()
        }
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                ACCESS_FINE_LOCATION_REQUEST_CODE)

        } else {
            mLocationPermisionGrated = true
            startServiceLocation()
        }
    }

    private fun startServiceLocation(){
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())
        locationRequest = LocationRequest()
        locationRequest.interval = 1000
        locationRequest.fastestInterval = 1000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult?: return

                for (location in locationResult.locations) {
                    updateMarkerLocation(LatLng(location.latitude, location.longitude))
                }
            }
        }

        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(KEY_CAMERA_POSITION, mMap?.cameraPosition)
        outState.putParcelable(KEY_LOCATION, lastKnowLocation)
        super.onSaveInstanceState(outState)
    }


    companion object {
        @JvmStatic
        val LATITUDE: String = "latitude"
        @JvmStatic
        val LONGITUDE: String = "longitude"
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MapFragment().apply {
//                arguments = Bundle().apply {
////                    putString(ARG_PARAM1, param1)
////                    putString(ARG_PARAM2, param2)
//                }
            }
    }
}
