package ni.org.untaxyclase.api

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiInterface {
    @POST("enviar-coordenadas")
    fun enviarCoordenadas(@Body coordinates: RequestBody): Call<String>
}