package ni.org.untaxyclase.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

class Api {
    companion object {

        @JvmStatic
        fun getRetrofitObject(): ApiInterface {
            val httpClient = OkHttpClient.Builder()
                .addInterceptor(BasicAuthInterceptor("vqhrRi7lUHxqydD1RO2uqXlwGqwMZzrm", ""))
            val builder = Retrofit.Builder()
                .baseUrl("http://192.168.1.17/aldea/credito2/web/api/")
                .addConverterFactory(ScalarsConverterFactory.create())
            val retrofit = builder.client(httpClient.build())
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}