package ni.org.untaxyclase

import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.pusher.client.channel.Channel
import com.pusher.client.channel.PusherEvent
import com.pusher.client.channel.SubscriptionEventListener
import ni.org.untaxyclase.api.Api
import ni.org.untaxyclase.ui.SharedViewModel.MapsCoordinatesSharedViewModel
import ni.org.untaxyclase.ui.maps.MapFragment
import retrofit2.Callback
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class NavigationDrawerActivity : AppCompatActivity(), SubscriptionEventListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var pusher: Pusher
    private lateinit var mapsCoordinatesSharedViewModel: MapsCoordinatesSharedViewModel//view model para compartir datos

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation_drawer)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send, R.id.nav_map
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        mapsCoordinatesSharedViewModel = ViewModelProviders.of(this).get(MapsCoordinatesSharedViewModel::class.java)

        configurePusher()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.navigation_drawer, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                val jsonObject = JSONObject()
                jsonObject.put("latitude", 14.14121412131)
                jsonObject.put("longitude", 13.234234234234242)

                val body =  RequestBody.create(
                    MediaType.parse("application/json"),
                    jsonObject.toString()
                )

                Api.getRetrofitObject().enviarCoordenadas(body).enqueue(object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>) {
                        Log.i("Success", "Success")
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        Log.e("failed", "failed")
                    }
                })
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun configurePusher(){
        val pusherOptions = PusherOptions()
        pusherOptions.setCluster("us2")
        pusher = Pusher("bd2b072b615654fcd6e8", pusherOptions)
        val channel: Channel = pusher.subscribe("my-channel")

        channel.bind("my-event", this)
        pusher.connect()
    }


    /**
     * Pusher event
     */
    override fun onEvent(event: PusherEvent?) {
        val data = event?.data!!
        mapsCoordinatesSharedViewModel.data.postValue(data)
    }

    override fun onPause() {
        super.onPause()
        pusher.disconnect()
    }

    override fun onResume() {
        super.onResume()
        pusher.connect()
    }

    override fun onDestroy() {
        super.onDestroy()
        pusher.disconnect()
    }
}
